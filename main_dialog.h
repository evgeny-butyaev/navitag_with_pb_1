#ifndef MAINDIALOG_H
#define MAINDIALOG_H

#include <QDialog>
#include <QTcpSocket>
#include <QByteArray>

#include "tracker.pb.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainDialog; }
QT_END_NAMESPACE

class MainDialog : public QDialog
{
	Q_OBJECT

public:
	MainDialog(QWidget *parent = nullptr);
	~MainDialog();

private slots:
	void on_btnConnect_clicked();
	void on_btnDisconnect_clicked();
	void on_btnSendPing_clicked();
	void on_btnSendGeolocation_clicked();

private:
	void on_socket_connected();
	void on_socket_disconnected();
	void on_socket_error(QAbstractSocket::SocketError err);
	void on_socket_ready_read();

private:
	NavitagClientMessage prepareClientMessage();

private:
	static quint16 crc16_append(quint16 crc, quint8 b) noexcept;
	static quint16 crc16(const void* data, std::size_t size) noexcept;

	void sendClientMessage(const NavitagClientMessage& cm);
	bool getNextBlock(QByteArray& block);

private:
	QTcpSocket socket_;
	QByteArray data_;

private:
	Ui::MainDialog *ui;
};
#endif // MAINDIALOG_H
