#include "main_dialog.h"
#include "./ui_main_dialog.h"

#include <QDebug>
#include <QHostAddress>
#include <QMessageBox>
#include <QString>
#include <QDateTime>

MainDialog::MainDialog(QWidget* parent)
	: QDialog(parent)
	, ui(new Ui::MainDialog)
{
	ui->setupUi(this);

	connect(&socket_, &QTcpSocket::connected, this, &MainDialog::on_socket_connected);
	connect(&socket_, &QTcpSocket::disconnected, this, &MainDialog::on_socket_disconnected);
	connect(&socket_, &QTcpSocket::errorOccurred, this, &MainDialog::on_socket_error);
	connect(&socket_, &QTcpSocket::readyRead, this, &MainDialog::on_socket_ready_read);
}

MainDialog::~MainDialog()
{
	delete ui;
}

void MainDialog::on_btnConnect_clicked()
{
	if (QTcpSocket::UnconnectedState != socket_.state())
	{
		qDebug() << "Socket already connected or tries to connect!";
		return;
	}

	qDebug() << "Connecting to host...";
	socket_.connectToHost(ui->editHost->text(), static_cast<quint16>(ui->spinPort->value()));
}

void MainDialog::on_btnDisconnect_clicked()
{
	if (QTcpSocket::UnconnectedState == socket_.state())
	{
		qDebug() << "Socket is not connected!";
		return;
	}

	qDebug() << "Disonnecting from host...";
	socket_.disconnectFromHost();
}

void MainDialog::on_socket_connected()
{
	data_.clear();
	QMessageBox::information(this, "Info", "Socket connected!");
}

void MainDialog::on_socket_disconnected()
{
	QMessageBox::information(this, "Info", "Connection lost!");
}

void MainDialog::on_socket_error(QAbstractSocket::SocketError)
{
	QMessageBox::critical(this, "Socket erro", socket_.errorString());
}

NavitagClientMessage MainDialog::prepareClientMessage()
{
	NavitagClientMessage cm;

	static qint64 message_id {1000};

	cm.set_deviceid(ui->editDeviceID->text().toStdString());
	cm.set_message_id(message_id++);

	auto* di {cm.mutable_device_profile()};
	di->set_app_version(ui->editAppVersion->text().toStdString());
	di->set_platform(ui->editPlatform->text().toStdString());
	di->set_supports_assistance_requests(true);

	return cm;
}

void MainDialog::on_btnSendPing_clicked()
{
	if (QTcpSocket::ConnectedState != socket_.state())
	{
		qDebug() << "Socket is not connected!";
		return;
	}

	auto cm {prepareClientMessage()};
	cm.set_is_ping(true);

	sendClientMessage(cm);
}

void MainDialog::on_btnSendGeolocation_clicked()
{
	if (QTcpSocket::ConnectedState != socket_.state())
	{
		qDebug() << "Socket is not connected!";
		return;
	}

	auto cm {prepareClientMessage()};
	auto& ni {*cm.add_track_point_info()->mutable_nav_info()};

	ni.set_captured_timestamp(QDateTime::currentDateTime().currentSecsSinceEpoch());
	ni.set_lat_deg(ui->spinLatitude->value());
	ni.set_lon_deg(ui->spinLongitude->value());
	ni.set_altitude_m(ui->spinAltitude->value());
	ni.set_speed_kmh(ui->spinSpeed->value());

	sendClientMessage(cm);
}

void MainDialog::sendClientMessage(const NavitagClientMessage& cm)
{
	static quint8 buf[256];
	auto size {cm.ByteSizeLong()};
	if ((1 > size) || ((sizeof(buf) - 8) < size))
	{
		qDebug() << "Invalid message size calculated: " << size;
		return;
	}

	if (!cm.SerializeToArray(buf + 8, static_cast<int>(size)))
	{
		QMessageBox::critical(this, "Error", "Cannot serialize object!");
		return;
	}

	quint16 crc = crc16(buf + 8, size);
	quint32 usize {static_cast<quint32>(size)};
	quint8* p {buf};

	// Version
	*(p++) = 0;
	*(p++) = 1;

	// Size
	*(p++) = static_cast<quint8>((usize >> 24) & 0xFF);
	*(p++) = static_cast<quint8>((usize >> 16) & 0xFF);
	*(p++) = static_cast<quint8>((usize >> 8) & 0xFF);
	*(p++) = static_cast<quint8>(usize & 0xFF);

	// crc
	*(p++) = static_cast<quint8>((crc >> 8) & 0xFF);
	*(p++) = static_cast<quint8>(crc & 0xFF);

	socket_.write(reinterpret_cast<const char*>(buf), size + 8);
}

quint16 MainDialog::crc16_append(quint16 crc, quint8 b) noexcept
{
	crc ^= static_cast<quint16>(b) << 8;
	for (std::size_t i = 0; i < 8; ++i)
	{
		if (0 != (crc & 0x8000))
		{
			crc <<= 1;
			crc ^= 0x1021;
		}
		else
		{
			crc <<= 1;
		}
	}
	return crc;
}

quint16 MainDialog::crc16(const void* data, std::size_t size) noexcept
{
	const auto* p {static_cast<const quint8*>(data)};
	quint16 crc {0};

	while (size--)
	{
		crc = crc16_append(crc, *(p++));
	}

	return crc;
}

void MainDialog::on_socket_ready_read()
{
	data_ += socket_.readAll();

	while (true)
	{
		QByteArray block;

		if (!getNextBlock(block))
		{
			socket_.disconnectFromHost();
			break;
		}

		if (block.isEmpty())
		{
			qDebug() << "Waiting for more data...";
			break;
		}

		// Try decode received block
		qDebug() << "Received valid block of " << block.size() << " byte(s)";

		NavitagServerMessage sm;
		if (!sm.ParseFromArray(block.data(), block.size()))
		{
			qDebug() << "Error parsing block as valid protobuf message!";
		}
		else
		{
			qDebug() << "Message from server:\n" << sm.DebugString().c_str();
		}
	}
}

bool MainDialog::getNextBlock(QByteArray& block)
{
	block.clear();

	if (8 > data_.size())
	{
		return true;
	}

	if (('\0' != data_[0]) || ('\1' != data_[1]))
	{
		qDebug() << "Invalid block version.";
		return false;
	}

	quint8* p {reinterpret_cast<quint8*>(data_.data() + 2)};

	std::size_t size {0};
	size |= static_cast<std::size_t>(*(p++)) << 24;
	size |= static_cast<std::size_t>(*(p++)) << 16;
	size |= static_cast<std::size_t>(*(p++)) << 8;
	size |= static_cast<std::size_t>(*(p++));

	if (0x10000 < size)
	{
		qDebug() << "Size in header (" << size << ") is too big!";
		return false;
	}

	if ((size + 8) > static_cast<size_t>(data_.size()))
	{
		return true;
	}

	quint16 crc_in_header {0};
	crc_in_header |= static_cast<qint16>(*(p++)) << 8;
	crc_in_header |= static_cast<qint16>(*(p++));

	if (crc16(p, size) != crc_in_header)
	{
		qDebug() << "CRC16 mismatch!";
		return false;
	}

	block.append(reinterpret_cast<const char*>(p), static_cast<int>(size));
	data_.remove(0, static_cast<int>(size + 8));

	return true;
}
